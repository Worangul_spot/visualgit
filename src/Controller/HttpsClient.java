package Controller;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLPeerUnverifiedException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.Certificate;

public class HttpsClient {
    public static void main(String[] args) {
        new HttpsClient().testIt();
    }

    private void testIt() {
        String https_url = "https://www.google.com/";
        URL url;
        try {
            url = new URL(https_url);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

            //dump all cert info
            print_https_cert(con);

            //dump all the content
            print_content(con);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void print_https_cert(HttpsURLConnection connection) {
        if (connection != null) {
            try {
                System.out.println("Response Code :" + connection.getResponseCode());
                System.out.println("Cipher Suite : " + connection.getCipherSuite());
                System.out.println("\n");
                Certificate[] certs = connection.getServerCertificates();
                for (Certificate cert : certs) {
                    System.out.println("Cert Type : " + cert.getType());
                    System.out.println("Cert Hash Code : " + cert.hashCode());
                    System.out.println("Cert Public Key Algorithm : " + cert.getPublicKey());
                    System.out.println("Cert Public Key Format : " + cert.getPublicKey().getFormat());
                    System.out.println("\n");
                }
            } catch (SSLPeerUnverifiedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void print_content(HttpsURLConnection connection)
    {
        if(connection!=null)
        {
            try
            {
                System.out.println("*****Content of the URL ******");
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(connection.getInputStream())
                );
                String input;

                while ((input = br.readLine()) != null)
                {
                    System.out.println(input);
                }
                br.close();
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        }
    }
    /*Output:
    Response Code : 200
Cipher Suite : SSL_RSA_WITH_RC4_128_SHA

Cert Type : X.509
Cert Hash Code : 7810131
Cert Public Key Algorithm : RSA
Cert Public Key Format : X.509

Cert Type : X.509
Cert Hash Code : 6042770
Cert Public Key Algorithm : RSA
Cert Public Key Format : X.509

****** Content of the URL ********
<!doctype html><html><head><meta http-equiv="content-type" ......
    */
}
