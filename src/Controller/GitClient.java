package Controller;

public class GitClient {
    /*
    *
    * String command = "curl -X GET https://postman-echo.com/get?foo1=bar1&foo2=bar2";
    * ProcessBuilder processBuilder = new ProcessBuilder(command.split(" "));
    * processBuilder.directory(new File("/home/"));
    * Process process = processBuilder.start();
    * InputStream inputStream = process.getInputStream();
    * --If we need to run additional commands, we can reuse the ProcessBuilder instance by passing new commands and arguments in a String array
    * processBuilder.command(new String[]{"curl", "-X", "GET", "https://postman-echo.com?foo=bar"});
    * --Finaly, to terminate each Process instance, we should use:
    * process.destroy();
    * --As an alternative to using the ProcessBuilder class, we can use Runtime.getRuntime() to get an instance of the Process class.
    * curl -X POST https://postman-echo.com/post --data foo1=bar&foo2=bar2
    * --Now, let's execute the command by using the Runtime.getRuntime() method:
    * String command ="curl -X POST https://postman-echo.com/post --data foo1=bar1";
    * Process process = Runtime.getRuntime().exec(command);
    * process.getInputStream();
    * process.destroy();
    */
    public void Authenticate(String username)
    {
        if(username!=null) {
            String command = "curl -u " + username + " https://api.github.com/user";
            ProcessBuilder processBuilder = new ProcessBuilder(command.split(" "));
        }
    }

}
