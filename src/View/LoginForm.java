package View;
import Controller.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.EventListener;

public class LoginForm
{
    private JFrame LoginFrame;
    private JTextArea LoginTextArea;
    private JButton LoginButton;
    private JPasswordField passwordField1;

    public LoginForm()
    {
        prepareGUI();
    }
    public static void main(String[] args)
    {
        LoginForm loginForm = new LoginForm();
        //loginForm.renderForm();
    }

    public void prepareGUI()
    {
        LoginTextArea = new JTextArea();
        LoginButton = new JButton();
        LoginFrame = new JFrame();
        LoginFrame.setSize(300, 200);
        LoginFrame.setLayout(new GridLayout(1,2));
        LoginFrame.add(LoginButton);
        LoginFrame.add(LoginTextArea);
        LoginFrame.setVisible(true);
        //loginForm.addWinListener();

    }

}

